This project is intended to be a "devcontainer" template for java projects,
with the latest version of maven-wrapper always commited. That way, there is
no need to install maven in the container, and a standard `eclipse-temurin`
docker image can be used as a starter to develop code.

Just download this project, or fork it, and use it.

Merge requests / update feedbacks always welcome.

Want to contribute: https://www.buymeacoffee.com/navneetkarnani